# smalloc

Malloc wrapper converting malloc() calls to filesystem-backed mmap-s. Particularly useful to call large processes on small VPSes.
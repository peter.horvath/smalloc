#!/usr/bin/make -f
CC:=gcc
CFLAGS:=-Wall -g -O0 -fPIC
RM:=rm

OBJS:=smalloc.o

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

libsmalloc.so: $(OBJS)
	$(CC) $(CFLAGS) -shared -ldl -o $@ $(OBJS)

test: test.c
	$(CC) -o test ./test.c

.PHONY: clean
clean:
	$(RM) -vf $(OBJS) libsmalloc.so

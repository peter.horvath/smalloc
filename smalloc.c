#define _GNU_SOURCE
#include <dlfcn.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define SMALLOC_SIZE_LIMIT 1048576
#define SMALLOC_TMP_DIR "/tmp"
#define PAGE_SIZE 4096

static void *(*orig_malloc)(size_t) = NULL;
static void *(*orig_calloc)(size_t, size_t) = NULL;
static void *(*orig_realloc)(void*, size_t) = NULL;
static void (*orig_free)() = NULL;

static volatile uint64_t next_id = 0;

static uint64_t size_limit = 0;
static char* tmp_dir = 0;

static void uint2hex(char* result, uint64_t in) {
  for (uint8_t n=0; n<16; n++) {
    uint8_t nibble = (in >> (n << 2)) & 0xf;
    char c;
    if (nibble < 10) {
      c = '0' + nibble;
    } else {
      c = nibble - 10 + 'a';
    }
    result[15-n] = c;
  }
  result[16] = 0;
}

static void debug(char* msg, uint64_t val) {
  write(2, msg, strlen(msg));
  write(2, " ", 1);
  char buf[17];
  uint2hex(buf, val);
  write(2, buf, strlen(buf));
  write(2, "\n", 1);
}

static void die(char* msg) {
  write(2, msg, strlen(msg));
  write(2, "\n", 1);
  exit(1);
}

static void get_size_limit() {
  if (size_limit) {
    return;
  }
  char* env = getenv("SMALLOC_SIZE_LIMIT");
  if (!env) {
    size_limit = SMALLOC_SIZE_LIMIT;
  } else {
    size_limit = atoll(env);
  }
}

static void get_tmp_dir() {
  if (tmp_dir) {
    return;
  }
  char *env = getenv("SMALLOC_TMP_DIR");
  if (!env) {
    tmp_dir = SMALLOC_TMP_DIR;
  } else {
    tmp_dir = env;
  }
}

static void int_to_path(char *path, uint64_t i) {
  get_tmp_dir();

  strcpy(path, tmp_dir);
  int l = strlen(path);
  uint2hex(path + l, i);
}

static void ptr_to_path(char *path, void* ptr) {
  int_to_path(path, (uint64_t)ptr);
}

static int int_exists(uint64_t i) {
  char path[PAGE_SIZE];
  struct stat stat_buf;
  int_to_path(path, ptr);
  int ret = stat(path, &stat_buf);
  if (ret == -1) {
    if (errno == ENOENT) {
      return 0;
    } else {
      die("stat results error, but not ENOENT");
    }
  } else {
    return 1;
  }
}

static int ptr_exists(void *ptr) {
  return int_exists((uint64_t)ptr);
}

static void int_rename_ptr(uint64_t i, void* ptr) {
  char path1[PAGE_SIZE];
  char path2[PAGE_SIZE];
  int_to_path(path1, i);
  ptr_to_path(path2, i);
  if (rename(path1, path2)) {
    die("can not rename");
  }
}

static void ptr_unlink(void *ptr) {
  char path[PAGE_SIZE];
  ptr_to_path(path, ptr);
  if (unlink(path)) {
    die("can not unlink");
  }
}

static int make_tmpfile(char *dst_path) {
  char path[PAGE_SIZE];
  for (;;) {
    uint64_t _id = __atomic_add_fetch(&next_id, 1, __ATOMIC_SEQ_CST);
    int_to_path(path, _id);
    int fd = open(path, O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
    if (fd == -1) {
      if (errno == EEXIST) {
        continue;
      } else {
        die("can not create temp file");
      }
    } else {
      strcpy(dst_path, path);
      return fd;
    }
  }
}

void* malloc(size_t size) {
  debug("malloc", size);
  if (!orig_malloc) {
    orig_malloc = dlsym(RTLD_NEXT, "malloc");
  }
  if (size >= SIZE_LIMIT) {
    return orig_malloc(size);
  } else {
    return orig_malloc(size);
  }
}

void *realloc(void *ptr, int n) {
  if (!orig_realloc) {
    orig_realloc = dlsym(RTLD_NEXT, "realloc");
  }
  if (fs_exists(ptr)) {
    
  } else {
    return orig_realloc(ptr, n);
  }
}

void free(void *p) {
  if (!orig_free) {
     orig_free = dlsym(RTLD_NEXT, "free");
  }
  orig_free(p);
}

void* calloc(size_t nmemb, size_t size) {
  if (!orig_calloc) {
    orig_calloc = dlsym(RTLD_NEXT, "calloc");
  }
  if (nmemb * size >= SIZE_LIMIT) {
    void *ret = malloc(nmemb * size);
    bzero(ret, nmemb * size);
    return ret;
  } else {
    return orig_calloc(nmemb, size);
  }
}
